import 'dart:math';
import 'package:flutter/material.dart';

// Object movement used by ObjectSpeedGame
class Movement {
  // Color value
  final int id;

  // Begin point
  Offset begin;

  // End point
  Offset end;

  // Transition duration
  Duration duration;

  // Transition distance
  double get distance => (end - begin).distance;

  // Transition speed
  double get speed => distance / duration.inSeconds;

  Movement(this.id) {
    duration = Duration(seconds: 1 + Random().nextInt(3));
    begin = _randomOffset;
    end = _randomOffset;
  }

  Offset get _randomOffset =>
      Offset(Random().nextInt(10).toDouble(), Random().nextInt(6).toDouble());
}
