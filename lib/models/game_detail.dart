import 'package:flutter/material.dart';
import 'package:quinb/models/game_category.dart';
import 'package:quinb/models/game_mode.dart';

// Game info
class GameDetails {
  // Game name
  String title;

  // Game short description
  String subtitle;

  // Game rules
  String rules;

  // Game category
  GameCategory category;

  // Corresponding GameMode
  GameMode mode;

  // Text displayed in-game (optional)
  String inGameTip;

  GameDetails({
    @required this.title,
    @required this.subtitle,
    @required this.rules,
    @required this.category,
    @required this.mode,
    this.inGameTip,
  });
}
