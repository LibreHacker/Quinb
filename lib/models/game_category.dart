import 'package:flutter/material.dart';

enum GameCategory { Logic, Sound, Vibration }

extension CategoryIcon on GameCategory {
  // Category name
  String get name => toString().split('.').last;

  // Category icon
  IconData get icon {
    if (this == GameCategory.Sound) return Icons.volume_up;
    if (this == GameCategory.Vibration) return Icons.vibration;
    if (this == GameCategory.Logic) return Icons.category;
    return Icons.error;
  }
}
