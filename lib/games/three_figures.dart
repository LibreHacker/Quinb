import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/models/figure.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/resources/colors.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/local_data_controller.dart';

// Find three identical figures
class ThreeFiguresGame extends BasicGame {
  ThreeFiguresGame(int players, GamePageState gamePage)
      : super(players, gamePage);

  @override
  bool get useTimer => false;

  // List of objects
  var objectList = List<Figure>(6);

  // Letters
  List<String> letters = ['O', 'X', 'I'];

  // Colors
  List<Color> colors = colorList.sublist(0, 3);

  @override
  void prePhase() async {
    _generateObjects();
    await Future.delayed(const Duration(seconds: 2));
  }

  @override
  void mainPhase() async {
    updater = Timer.periodic(
        Duration(milliseconds: 1000 + (5 - settings.difficulty.index) * 100),
        (_) {
      if (!isMainPhase || isPostPhase) return;
      _generateObjects();
      gamePage.refresh();
    });
  }

  void _generateObjects() {
    for (var i = 0; i < objectList.length; i++) {
      objectList[i] = Figure(letters[Random().nextInt(letters.length)],
          colors[Random().nextInt(colors.length)]);
    }
    var count = <Figure, int>{};
    objectList
        .forEach((o) => count[o] = !count.containsKey(o) ? 1 : count[o] + 1);
    correctValue = count.values.any((v) => v >= 3) ? Option.tap.value : 0;
  }

  @override
  Widget widget() => isMainPhase || isPostPhase
      ? Wrap(
          children: [
            InkWell(
              onTap: isMainPhase ? pause : null,
              child: GridView.count(
                shrinkWrap: true,
                crossAxisCount: 3,
                children: List.generate(6, (index) {
                  return Container(
                    decoration: BoxDecoration(
                      color: objectList[index].color,
                      border: Border.all(color: Colors.white),
                    ),
                    child: Center(
                      child: Text(
                        objectList[index].text,
                        style: const TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  );
                }),
              ),
            ),
          ],
        )
      : super.widget();
}
