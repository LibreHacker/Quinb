import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/color_operators.dart';
import 'package:quinb/util/local_data_controller.dart';
import 'package:quinb/util/random_color.dart';

// Math calculation game and Color calculation game
class CalculationGame extends BasicGame {
  // Math or Color game
  final bool colorMode;

  CalculationGame(int players, GamePageState gamePage, {this.colorMode = false})
      : super(players, gamePage);

  @override
  String get tip =>
      '$firstValue ${colorMode ? '+' : (multiplication ? 'x' : '÷')} $secondValue = ?';

  // Operands
  int firstValue;
  int secondValue;

  // Maximum operand value
  int maxValue;

  // If true: *, else /
  bool multiplication = true;

  @override
  void init() {
    maxValue = colorMode
        ? 0x000000FF - settings.difficulty.index * 0x00000022
        : 10 + settings.difficulty.index * 5;
    super.init();
  }

  @override
  void prePhase() async {
    if (colorMode) {
      firstValue = Random().nextColorValue(0x000000FF);
      secondValue = Random().nextColorValue(0x000000FF);
      correctValue = (Color(firstValue) + Color(secondValue)).value;
      optionsAvailable = [
        correctValue,
        for (var i = 0; i < min(4, settings.difficulty.index + 1); i += 1)
          (Random().nextBool()
                  ? Color(firstValue) + Color(Random().nextColorValue(maxValue))
                  : Color(firstValue) -
                      Color(Random().nextColorValue(maxValue)))
              .value
      ].toOptions(asColors: true)
        ..shuffle();
    } else {
      multiplication = Random().nextBool();
      secondValue = 1 + Random().nextInt(maxValue);
      if (multiplication) {
        firstValue = Random().nextInt(maxValue);
        correctValue = firstValue * secondValue;
      } else {
        correctValue = Random().nextInt(maxValue);
        firstValue = secondValue * correctValue;
      }
      optionsAvailable = [
        correctValue,
        for (var i in <int>[3, 5, 7, 10])
          max(
              correctValue +
                  (correctValue >= 10 && Random().nextBool() ? -1 : 1) * i,
              0)
      ].toOptions()
        ..shuffle();
    }
    await Future.delayed(const Duration(seconds: 2));
  }

  @override
  Widget widget() => colorMode && (isMainPhase || isPostPhase)
      ? Expanded(
          child: InkWell(
            onTap: isMainPhase ? pause : null,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                CircleAvatar(backgroundColor: Color(firstValue ?? 0)),
                const Text('+', style: TextStyle(fontSize: 30)),
                CircleAvatar(backgroundColor: Color(secondValue ?? 0)),
              ],
            ),
          ),
        )
      : super.widget();
}
