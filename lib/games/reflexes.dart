import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/resources/colors.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/local_data_controller.dart';

// Reflexes game
class ReflexesGame extends BasicGame {
  ReflexesGame(int players, GamePageState gamePage) : super(players, gamePage);

  // Time the color stay on
  final int _time = 400 + (6 - settings.difficulty.index) * 100;

  // Colors used
  final int colorCount = min(1 + settings.difficulty.index, 4);

  @override
  void prePhase() async {
    correctValue = Colors.white.value;
    optionsAvailable = [
      for (var i = 0; i < colorCount; i += 1) colorList[i].value
    ].toOptions(asColors: true);
    await Future.delayed(const Duration(seconds: 2));
  }

  @override
  void mainPhase() async {
    updater = Timer.periodic(
        Duration(
            milliseconds:
                2000 + (Random().nextInt(settings.timeAvailable - 2)) * 1000),
        (_) async {
      if (!isMainPhase || isPostPhase) return;
      correctValue = colorList[Random().nextInt(colorCount)].value;
      gamePage.refresh();
      await Future.delayed(Duration(milliseconds: _time));
      if (!isPostPhase) {
        correctValue = Colors.white.value;
        gamePage.refresh();
      }
    });
  }

  @override
  Widget widget() => isMainPhase || isPostPhase
      ? Expanded(
          flex: 2,
          child: InkWell(
              onTap: isMainPhase ? pause : null,
              child: Container(color: Color(correctValue))))
      : super.widget();
}
