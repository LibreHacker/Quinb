// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          'Increase system volume in order to play all the games':
              'Aumenta il volume del dispositivo per poter giocare a tutti i giochi',
        }
      };

  String get i18n => localize(this, _t);
}
