import 'package:flutter/material.dart';

class PageButton extends StatelessWidget {
  final String title;
  final VoidCallback onPressed;

  PageButton({
    @required this.title,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      height: 50,
      margin: const EdgeInsets.only(bottom: 25),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.black45),
          borderRadius: const BorderRadius.all(Radius.circular(30))),
      child: FlatButton(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(30))),
        child: Text(title, style: TextStyle(color: Colors.black, fontSize: 21)),
        onPressed: onPressed,
      ),
    );
  }
}
