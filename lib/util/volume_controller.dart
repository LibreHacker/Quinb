import 'package:volume/volume.dart';

// Current volume
int currentVol;

// Maximum volume
int maxVol;

// Init volume controller and read volume info
Future<void> initVolumeController() async {
  await Volume.controlVolume(AudioManager.STREAM_MUSIC);
  await getVol();
}

// Update volume info
Future<void> getVol() async {
  currentVol = await Volume.getVol;
  maxVol = await Volume.getMaxVol;
}

// Set the volume
Future<void> setVol(int newValue) async {
  await Volume.setVol(newValue, showVolumeUI: ShowVolumeUI.SHOW);
}
