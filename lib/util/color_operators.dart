import 'package:flutter/material.dart';

// Add + and - operators to Color
extension ColorOperators on Color {
  Color operator +(Color c) => Color.fromARGB(
        0xFF,
        (red + c.red) ~/ 2,
        (green + c.green) ~/ 2,
        (blue + c.blue) ~/ 2,
      );

  Color operator -(Color c) => Color.fromARGB(
        0xFF,
        red * 2 - c.red,
        green * 2 - c.green,
        blue * 2 - c.blue,
      );
}
