# Italian translation for Quinb 1.0.0
# Copyright (C) 2020 DeepDaikon
# This file is distributed under the same license as Quinb.

#
msgid ""
msgstr ""
#: lib/ui/screens/select/select_page.dart:
msgid "GAMES"
msgstr "GIOCHI"

#: lib/ui/screens/select/select_page.dart:
msgid "Rules"
msgstr ""

#: lib/ui/screens/select/select_page.dart:
msgid "Sound games"
msgstr ""

#: lib/ui/screens/select/select_page.dart:
msgid "Sound-based games enabled"
msgstr ""

#: lib/ui/screens/select/select_page.dart:
msgid "Sound-based games disabled"
msgstr ""

#: lib/ui/screens/select/select_page.dart:
msgid "players"
msgstr ""

#: lib/ui/screens/select/select_page.dart:
msgid "player"
msgstr ""

#: lib/ui/screens/select/select_page.dart:
msgid "Settings"
msgstr ""

#: lib/ui/screens/select/select_page.dart:
msgid "Info"
msgstr ""

#: lib/ui/screens/select/select_page.dart:
msgid "All"
msgstr ""

