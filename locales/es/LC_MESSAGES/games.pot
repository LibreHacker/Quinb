# Spanish translation for Quinb 1.0.0
# Copyright (C) 2020 DeepDaikon
# This file is distributed under the same license as Quinb.

#
msgid ""
msgstr ""
#: lib/games/abstract_basic.dart:
msgid "Too many rounds"
msgstr ""

#: lib/games/abstract_basic.dart:
msgid "\nYou lost!"
msgstr ""

#: lib/games/abstract_basic.dart:
msgid "Too many mistakes. "
msgstr ""

#: lib/games/abstract_basic.dart:
msgid "\nYou lost!"
msgstr ""

#: lib/games/abstract_basic.dart:
msgid "won!"
msgstr ""

#: lib/games/abstract_basic.dart:
msgid "Nobody won!"
msgstr ""

#: lib/games/abstract_basic.dart:
msgid "won!"
msgstr ""

#: lib/games/abstract_basic.dart:
msgid "You won!"
msgstr ""

#: lib/games/abstract_basic.dart:
msgid "P A U S E"
msgstr ""

#: lib/games/color_used.dart:
msgid "What color is the text?"
msgstr ""

#: lib/games/color_used.dart:
msgid "What color is the background?"
msgstr ""

#: lib/games/color_used.dart:
msgid "What color is written?"
msgstr ""

#: lib/games/farm.dart:
msgid "Tap when you hear a"
msgstr ""

#: lib/games/recognize.dart:
msgid "Tap here to listen to the sample"
msgstr ""

#: lib/games/recognize.dart:
msgid "Tap here to start"
msgstr ""

#: lib/games/recognize.dart:
msgid "Tap when you hear the previous sequence"
msgstr ""

#: lib/games/recognize.dart:
msgid "Tap when you hear the previous sound"
msgstr ""

#: lib/games/remember.dart:
msgid "What color was the "
msgstr ""

#: lib/games/remember.dart:
msgid "What letter was in the"
msgstr ""

#: lib/games/remember.dart:
msgid "figure?"
msgstr ""

#: lib/games/remember.dart:
msgid "1st"
msgstr ""

#: lib/games/remember.dart:
msgid "2nd"
msgstr ""

#: lib/games/remember.dart:
msgid "3rd"
msgstr ""

#: lib/games/remember.dart:
msgid "4th"
msgstr ""

#: lib/games/remember.dart:
msgid "5th"
msgstr ""

#: lib/games/remember.dart:
msgid "6th"
msgstr ""

#: lib/games/shortest_vibration.dart:
msgid "First"
msgstr ""

#: lib/games/shortest_vibration.dart:
msgid "Second"
msgstr ""

#: lib/games/shortest_vibration.dart:
msgid "Third"
msgstr ""

#: lib/games/shortest_vibration.dart:
msgid "Fourth"
msgstr ""

