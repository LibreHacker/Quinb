# Quinb

Quinb is a multiplayer reaction game: train your mind and your reflexes while having fun.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/xyz.deepdaikon.quinb/)

## Description

Quinb is a reaction / logic game for up to 4 players on the same device.

It contains many minigames where you have to answer questions as fast as you can to get a point.

These games are based on 3 different categories:

* Logic: games that require intuition, logic and fast reflexes
* Audio: sound-based games, you have to listen carefully to find out the correct answer
* Vibration: vibration-based games that require you to listen carefully to the vibrations of your device

You can play alone if you want, but it is more fun to play with friends of all ages. It's great if you are stuck with nothing to do while you're with friends.

If you think you're fast, challenge and beat your friends!

## Screenshots

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/HomePage.png" height="320">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/Screen_1.png" height="320">
<img src ="fastlane/metadata/android/en-US/images/phoneScreenshots/Screen_2.png" height="320">
